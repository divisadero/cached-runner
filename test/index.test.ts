import {CachedRun} from '../src';

/**
 * Runs a function that delays for certain number of millis
 * @param {number} millis
 * @returns {Promise<any>}
 */
async function delay(millis: number) {
    return new Promise(resolve => setTimeout(() => {resolve()}, millis));
}

describe('CachedRun', () => {
    const data = 'hello';
    const period = 50;
    const request = () => Promise.resolve(data);
    const process = data => data + Math.random();
    const always = data => data + 2;
    const cachedRun = new CachedRun(period, request, process, always);

    describe('CachedRun#start', () => {
        it('should start fresh data', async () => {
            const expected = await cachedRun.start();
            const response = await cachedRun.start();
            expect(response).toEqual(expected);
        });

        it('should wait for the cache to expire', async () => {
            const expected = await cachedRun.start();
            await delay(80);
            const response = await cachedRun.start();
            expect(response).not.toEqual(expected);
        });
    });

    describe('CachedRun#refresh', () => {
        const cachedRun = new CachedRun(period, request, process, always);
        it('should refresh before cache expires', async () => {
            const expected = await cachedRun.start();
            cachedRun.refresh();
            const response = await cachedRun.start();
            expect(response).not.toEqual(expected);
        });
    });
});
