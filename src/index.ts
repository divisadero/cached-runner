/**
 * CachedRun helps you to make recurrent requests to the same resource without
 * having to hit it several times. It does so by caching the result and passing
 * request through only when the cache expires. It also counts on with a couple
 * of helper functions that may come handy when you have to transform this raw
 * data.
 * @class CachedRun
 */
export class CachedRun<CacheType> {
    private lastCall: number = 0;
    private cache: CacheType;

    /**
     * Constructor for the class. Receives a period and a request function to run.
     * @param {number} period - Number of millis to keep the cache.
     * @param {function} request - Function that returns the requested value.
     * @param {function} [process] - Run data from the request through this function before caching.
     * @param {function} [always] - Run data through this allways at the end of the pipeline.
     */
    constructor(
        private period: number,
        private request: () => Promise<CacheType | any>,
        private process: (data: any) => CacheType = el => el,
        private always: (data: CacheType) => any = el => el,
    ) {}

    /**
     * Run the pipeline according to the configuration.
     * @return {Promise} Data run through all the pipeline.
     */
    public start(): Promise<any> {
        return (+new Date() - this.lastCall > this.period)
            ? this.getFreshResults().then(res => this.cacheResults(res))
            : this.cacheResults();
    }

    /**
     * Forces the next run to refresh the cache
     */
    public refresh(): void {
        this.lastCall = 0;
    }

    /**
     * Run the request function and the postprocess function (if there's one defined).
     * @return {Promise} Promise with the requested values already processed.
     */
    private getFreshResults(): Promise<any> {
        return Promise
            .resolve(this.request())
            .then(res => this.process(res));
    }

    /**
     * Stores cache and manages timer.
     * @param {Object} [results] - Results from the request and further postprocess.
     * @return {Promise} Promise with the cached results processed.
     */
    private cacheResults(results?: CacheType): Promise<CacheType> {
        if (results) {
            this.cache = results;
            this.lastCall = +new Date();
        }
        return Promise
            .resolve(this.cache)
            .then(data => this.always(data));
    }
}
